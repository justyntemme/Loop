package main

import (
	"strings"
	"testing"
)

//TestRun tests the Run function of the main program loop
func TestRun(t *testing.T) {
	output := run("echo testing")
	if !(strings.Contains(output, "testing")) {
		t.Errorf("Output of Run did not match expected output. Output was '" + output + "'")
	}
}
