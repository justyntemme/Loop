package main
// :D
import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
)

var command string

const usage = "Loop through the given command passed via -c or --command= $COMMAND'"

func init() {
	flag.StringVar(&command, "c", "", usage)
	flag.StringVar(&command, "command", "", usage)
}
func main() {
	flag.Parse()
	fmt.Println(command)
	if command != "" {
		for {
			run(command)
		}
	} else {
		fmt.Println(usage)
	}
}

func run(command string) string {
	var outb bytes.Buffer
	cmd := exec.Command("bash", "-c", command)
	//Direct cmd.Stdout to another bytes buffer to trick the os/exec package to run cmd multiple times / as well as for testing purposes
	cmd.Stdout = &outb
	//Pipe Stdin to cmd.Stdin for ctl + c functionality
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		log.Fatal(err)
	}
	cmd.Wait()
	//Print command output
	fmt.Println(outb.String())
	//Return output for unit testing
	return string(outb.String())
}
